package producto;

import java.util.Arrays;

public class Ejercicio4 {

	public static void main(String[] args) {
		
		int array[] = new int[10];
		int contadorPares = 0, contadorImpar = 0;
		
		
		for (int i = 0; i < array.length; i++) {
			array[i] = i+1;
		}
		
		for (int i = 0; i < array.length; i++) {
			if ((i+1) % 2 == 0) {
				array[i]=0;
			}
		}
		
		System.out.println(Arrays.toString(array));
		
		for (int i = 0; i < array.length; i++) {
			if (array[i] == 0) {
				contadorPares++;
			} else {
				contadorImpar++;
			}
		}
		
		System.out.println("Ceros = " + contadorPares + ". Distintos de cero = " + contadorImpar + ".");
	}

}
